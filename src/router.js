import Vue from 'vue';
import Router from 'vue-router';

import {store} from './store/store'

import Recetas from './components/Recetas.vue';
import Receta from './components/Receta.vue';
import Ingredientes from './components/Ingredientes.vue';
import Categorias from './components/Categorias.vue';
import Grupos from './components/Grupos.vue';
import Login from './components/Login.vue';
import Registrar from './components/Registrar.vue';
import Perfil from './components/Perfil.vue';
import Landing from './components/LandingPage.vue';
Vue.use(Router);

export const router = new Router({
    mode: "history",
    routes :[
        {path:'/', component: Landing},
        {path:'/Login', component: Login},
        {path:'/registrar', component: Registrar},
        {path:'/perfil', component: Perfil},
        {path:'/recetas', component: Recetas},
        {path:'/receta', component: Receta},
        {path:'/categorias', component: Categorias},
        {path:'/grupos', component: Grupos},
        {path:'/ingredientes', component: Ingredientes},
        { path: '*', redirect: '/' }
    ]
});

router.beforeEach((to, from, next) => { 
    const publicPages = ['/', '/Registrar', '/Login'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = store.state.user.loggedIn;
    if (authRequired && !loggedIn) {
        return next('/Login');
    }
    next();
})