import Vue from 'vue'
import App from './App.vue'
import VueRosurce from 'vue-resource'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import {router} from './router'
import {store} from './store/store'

import VueSimpleAlert from "vue-simple-alert";


Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueSimpleAlert);
Vue.use(VueRosurce);

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
})
