import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";


Vue.use(Vuex);

export const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        user : {
            id: "",
            username: "",
            email: "",
            jwt:"",
            loggedIn:null
        },
        receta: {
            id: '',
            nombre:'',
            observaciones:'',
            raciones:'',
            tiempo:'',
            id_usuario:'',
            id_grupo:'',
            accion:''
        }
    },

    getters:{
        
    },
    mutations:{
        setUser : (state, payload) => {
            state.user.id = payload.id;
            state.user.username = payload.username;
            state.user.email = payload.email;
            state.user.jwt = payload.jwt;
            state.user.loggedIn = true;
            state.user.loggedOut = false;
            state.user.deleted = false;
        },
        setReceta : (state, payload) => {
            state.receta.id = payload.id;
            state.receta.nombre = payload.nombre;
            state.receta.observaciones = payload.observaciones;
            state.receta.raciones = payload.raciones;
            state.receta.tiempo = payload.tiempo;
            state.receta.id_usuario = payload.id_usuario;
            state.receta.id_grupo = payload.id_grupo;
            
        },
        setAccion : (state, payload) => {
            state.receta.accion = payload;
            
        },
        updateJWT : (state, payload) => {
            state.user.jwt = payload.jwt;
        },
        logOutUser : (state) => {
            state.user.id = "";
            state.user.username = "";
            state.user.email = "";
            state.user.jwt = "";
            state.user.loggedIn = false;
        },
        DeleteUser : (state) => {
            state.user.id = "";
            state.user.username = "";
            state.user.email = "";
            state.user.jwt = "";
            state.user.loggedIn = false;
        }
    },
    actions:{
        setUser : (context, payload) =>{
            context.commit('setUser', payload)
        },
        setReceta : (context, payload) =>{
            context.commit('setReceta', payload)
        },
        setAccion : (context, payload) =>{
            context.commit('setAccion', payload)
        },
        updateJWT : (context, payload) =>{
            context.commit('updateJWT', payload)
        },
        logOutUser : (context) =>{
            context.commit('logOutUser')
        },
        DeleteUser : (context) =>{
            context.commit('DeleteUser')
        }
    }

});
