import VueSimpleAlert from "vue-simple-alert";
function Expired (t) { 
    //alert("Su sesión ha expirado");
    t.$store.dispatch('logOutUser');
    t.$router.replace("/Login");
    t.$alert("Su sesión ha expirado");
}

export {Expired}